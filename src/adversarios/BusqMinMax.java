/**
 * Clase BusqMinMax que extiende la BusquedaA para realizar busqueda con adversarios utilizando el algoritmo minimax
 */
package adversarios;

import java.util.List;

/**
 * @param Estado, el tipo de los estados del juego
 * @param Accion, el tipo de las acciones aplicables en un estado (movimientos)
 * @param Jugador, el tipo de los jugadores
 * @author ines
 * @version 2019.11
 */
public class BusqMinMax<Estado,Accion,Jugador> extends BusquedaA<Estado,Accion,Jugador>{

	private Jugador max;
	/**
	 * Constructor
	 * @param juego, el Juego a jugar
	 */
	public BusqMinMax( Juego<Estado,Accion,Jugador> juego ) {
		super( juego );
	}

	/* (non-Javadoc)
	 * @see adversarios.BusquedaA#decideJugada(java.lang.Object, java.lang.Object)
	 */
	public Accion decideJugada( Estado e ) {
		this.max = getJuego().jugador(e); // max es el jugador al que le toca mover en el estado e
		List<Accion> acciones = getJuego().acciones(e); // posibles acciones/movimientos en el estado e
		// TODO
		// hay que encontrar la accion que proporciona maxima utilidad al jugador max
		// usando el algoritmo minimax
		
		Accion argMax=acciones.get(0);
		double maxValor=Double.NEGATIVE_INFINITY;

		for(Accion a:acciones) {
			double minValor=minValor(getJuego().resultado(e, a));
			if(minValor>maxValor) {
				argMax=a;
				maxValor=minValor;
			}
		}
		return argMax;
		
	}
	

	/**
	 * Metodo que calcula el mejor movimiento para el jugador rival (el que mas perjudica a la maquina)
	 * y retorna la utilidad de ese movimiento.
	 * @param e estado en el que calcular el movimiento
	 * @param max jugador que representa a la maquina
	 * @return utilidad del movimiento calculado
	 */
	public double minValor(Estado e ) {
		if(getJuego().terminalTest(e)) {
			return getJuego().utilidad(e, max);
		}
		double v=Double.POSITIVE_INFINITY;
		for(Accion a:getJuego().acciones(e)) {
			v=Math.min(v,maxValor(getJuego().resultado(e, a)));
		}
		return v;
	}
	
	/**
	 * Metodo que calcula el mejor movimiento para la maquina en un estado determinado
	 * y retorna la utilidad de ese movimiento.
	 * @param e estado en el que calcular el movimiento
	 * @param max jugador que representa a la maquina
	 * @return utilidad del movimiento calculado
	 */
	public double maxValor(Estado e) {
		if(getJuego().terminalTest(e)) {
			return juego.utilidad(e, max);
		}
		double v=Double.NEGATIVE_INFINITY;
		for(Accion a:getJuego().acciones(e)) {
			v=Math.max(v,minValor(getJuego().resultado(e, a)));
		}
		return v;
	}
	
	
}
	